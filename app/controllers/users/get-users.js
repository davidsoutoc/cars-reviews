'use strict';

const createJsonError = require('../../errors/create-json-error');
const { findAllUsers } = require('../../repositories/users-repository');
const { isAdmin } = require('../../helpers/utils');

async function getUsers(req, res) {
  try{
    const { rol } = req.auth;

    isAdmin(rol);

    const users = await findAllUsers();

    res.status(200);
    res.send(users);
  } catch(error) {
    createJsonError(error, res);
  }
}

module.exports = { getUsers };

'use strict';
const axios = require('axios');
const createJsonError = require('../../errors/create-json-error');
const { getRandomUser } = require('../../repositories/api-repository');

async function randomUser(req, res) {
  try {
    console.log('aki');
    const randomUser = await getRandomUser();
    console.log(randomUser);
    // Desestructuring usuario
    const [user] = randomUser.data.results;
    // Destructuring de los campos user;
    const { name, email, login, picture} = user;
    res.status(200);
    // Envio resultado
    res.send({
      name: `${name.first} ${name.last}`,
      username: login.username,
      password: login.password,
      email,
      photo: picture.large,
    });
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { randomUser };

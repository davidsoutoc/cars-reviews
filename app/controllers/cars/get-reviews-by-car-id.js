'use strict';

const Joi = require('joi');
const createJsonError = require('../../errors/create-json-error');
const throwJsonError = require('../../errors/throw-json-error');
const { findCarById, findReviewsByCarId } = require('../../repositories/cars-repository');
//const { findReviewsByCarId } = require('../../repositories/reviews-repository');

const schema = Joi.number().positive();

/**
 * Devuelve las Reviews para un carId determinado
 * GET /api/v1/cars/:id/reviews
 * @param  {Object} req
 * @param  {Object} res
 * @returns {Object}
 */
async function getCarReviewsById(req, res) {
  try {
    // Recuperamod el id de la URL. Lo hemos llamado idCar en el carsRoutes.js
    const { idCar } = req.params;
    // Validamos el idCar
    await schema.validateAsync(idCar);
    // Comprobamos si existe el coche con ese idCar
    const car = await findCarById(idCar);
    if (!car) {
      throwJsonError('El coche no existe', 400);
    }

    // Tenemos 2 opciones para recuperar esta información:
    // 1. Devolver solo las reviews para ese id:
    //const reviews = await findReviewsByCarId(id);
    // 2. Devolver la reviews con la información del coche incluida:
    const reviews = await findReviewsByCarId(idCar);

    res.status(200);
    res.send(reviews);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { getCarReviewsById };

'use strict';
const Joi = require('joi');
const createJsonError = require('../../errors/create-json-error');
//const { findCarById, removeCarById } = require('../../repositories/cars-repository');
const { findCarById, logicRemoveCarById } = require('../../repositories/cars-repository');
const schema = Joi.number().integer().positive().required();

async function deleteCarById(req, res) {
  try {
    const { idCar } = req.params;
    await schema.validateAsync(idCar);

    const car = await findCarById(idCar);
    if (!car) {
      throw new Error('Coche no existe');
    }
    await logicRemoveCarById(idCar);
    //Con Mysql podremos lanzar un error si no existe el idCar;

    res.status(204);
    res.end();
    //res.status(204).end();
    //res.status(200).send({message:`${idCar} borrado correctamente!`});
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { deleteCarById };
'use strict';

const Joi = require('joi');
const createJsonError = require('../../errors/create-json-error');
const throwJsonError = require('../../errors/throw-json-error');
const { isAdmin } = require('../../helpers/utils');
const {
  findCarByBrandAndModel,
  findCarById,
  updateCar
} = require('../../repositories/cars-repository');

const schemaId = Joi.number().positive().required();

const schema = Joi.object().keys({
  marca: Joi.string().min(3).max(20).required(),
  modelo: Joi.string().min(2).max(220).required(),
  anho: Joi
    .number()
    .integer()
    .positive()
    .min(1950)
    .max(new Date().getFullYear()),
  motor: Joi.string().valid('Diesel', 'Gasolina','Híbrido', 'Eléctrico'),
  cv: Joi.number().integer().positive().min(60).max(500)
});

async function updateCarById(req, res) {
  try {
    // Recuperamos el rol del JWT que viene en el Authorization
    const { rol } = req.auth;
    // Comprobamos el rol
    isAdmin(rol);
    // Obtenemos el idCar, nombre de variable puesto en el cars-routes.js
    const { idCar } = req.params;
    // Validamos el idCar
    await schemaId.validateAsync(idCar);

    // Comprobamos que exites el coche
    const car = await findCarById(idCar);
    if( !car ) {
      throwJsonError('Coche no existe', 400);
    }
    // Cogemos del body el objeto con los cambios
    const { body } = req;
    // Validamos el body
    await schema.validateAsync(body);

    const { marca, modelo } = body;
    // Exigimos que no exista un coche con la misma marca/modelo.
    const existCar = await findCarByBrandAndModel(marca, modelo);
    // Si existe un coche y ese id es distinto al id del coche actual es q ya existe esa marca/modelo
    if (existCar && existCar.id !== +idCar) {
      throwJsonError(`Ya existe ese modelo de coche en la aplicación ID: ${existCar.id}`, 409);
    }

    // Actualizamos el coche
    await updateCar(idCar, body);

    // Devolvemos que todo fue bien con un 204 - NO CONTENT
    res.status(204)
    res.end();
  } catch(error) {
    createJsonError(error, res);
  }
}

module.exports = { updateCarById };

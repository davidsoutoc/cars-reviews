'use strict';
const Joi = require('joi');
const createJsonError = require('../../errors/create-json-error');
const { isAdmin } = require('../../helpers/utils');
const {
  addCar,
  findCarByBrandAndModel
} = require('../../repositories/cars-repository');

const schema = Joi.object().keys({
  marca: Joi.string().min(3).max(20).required(),
  modelo: Joi.string().min(2).max(220).required(),
  anho: Joi
    .number()
    .integer()
    .positive()
    .min(1950)
    .max(new Date().getFullYear()),
  motor: Joi.string().valid('Diesel', 'Gasolina','Híbrido', 'Eléctrico'),
  cv: Joi.number().integer().positive().min(60).max(500)
});

async function createCar(req, res) {
  try {
    const { rol } = req.auth;
    isAdmin(rol);

    const { body } = req;
    const { marca, modelo } = body;

    const existCar = await findCarByBrandAndModel(marca, modelo);
    if (existCar) {
      const error = new Error('Ya existe ese modelo de coche en la aplicación');
      error.status = 409;
      throw error;
    }

    await schema.validateAsync(body);
    await addCar(body);

    res.status(201);
    res.end();
  } catch (error) {
    createJsonError(error, res);
  }
}

module.exports = { createCar };
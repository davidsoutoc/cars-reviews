'use strict';

const Joi = require('joi');
const createJsonError = require('../../errors/create-json-error');
const throwJsonError = require('../../errors/throw-json-error');
const { isAdmin } = require('../../helpers/utils');
const {
  findCarByBrandAndModel,
  findCarById,
  updateCar
} = require('../../repositories/cars-repository');

const schemaId = Joi.number().positive().required();

// La diferencia con el PUT es que aki ningún campo es obligatorio
const schema = Joi.object().keys({
  marca: Joi.string().alphanum().min(3).max(20),
  modelo: Joi.string().alphanum().min(2).max(220),
  anho: Joi
    .number()
    .integer()
    .positive()
    .min(1950)
    .max(new Date().getFullYear()),
  motor: Joi.string().valid('Diesel', 'Gasolina','Híbrido', 'Eléctrico'),
  cv: Joi.number().integer().positive().min(60).max(500)
});

async function patchCarById(req, res) {
  try {
    // Recuperamos el rol del JWT que viene en el Authorization
    const { rol } = req.auth;
    // Comprobamos si es admin
    isAdmin(rol);

    const { idCar } = req.params;
    // 1. validar id
    await schemaId.validateAsync(idCar);

    // 2. Validamos que existe el id seleccionado
    const car = await findCarById(idCar);
    if (!car) {
      throwJsonError('Coche no existe', 400);
    }

    //Validamos el body
    const { body } = req;
    await schema.validateAsync(body);
    // Formamos el objeto car con los campos original y los nuevos campos actualizados
    const updatedCar = {
      ...car,
      ...req.body,
    };

    const { marca, modelo } = updatedCar;
    // Exigimos que no exista un coche con la misma marca/modelo.
    const existCar = await findCarByBrandAndModel(marca, modelo);
    // Si existe un coche y ese id es distinto al id del coche actual es q ya existe esa marca/modelo
    if (existCar && existCar.id !== +idCar) {
      throwJsonError(`Ya existe ese modelo de coche en la aplicación ID: ${existCar.id}`, 409);
    }

    // Actualizamos coche
    await updateCar(idCar, updatedCar);

    res.status(200).send({ ...updatedCar });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { patchCarById };

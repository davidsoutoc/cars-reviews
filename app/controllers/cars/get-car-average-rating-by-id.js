'use strict';

const Joi = require('joi');

const { findCarById } = require('../../repositories/cars-repository');

const {
  findReviewsByCarId,
  getAverageRating,
  getNumberOfRatings
}  = require('../../repositories/reviews-repository');
const createJsonError = require('../../errors/create-json-error');
const throwJsonError = require('../../errors/throw-json-error');

const schema = Joi.number().positive();

async function getCarAverageRatingById(req, res) {
  try {
    // Con destructuring obtenemos el idCar de la URL
    const { idCar } = req.params;
    // Validamos el idCar con Joi
    await schema.validateAsync(idCar);

    // Comprobamos que existe un coche para ese idCar
    const car = await findCarById(idCar);
    if (!car) {
      throwJsonError('Coche no existe', 400);
    }

    // DOS FORMAS DE HACERLO de calcular la media de valoración
    // PRIMERA - Consulta SQL
    const rating = await getAverageRating(idCar);

    // console.log('Rating database', rating);
    const numValoraciones = await getNumberOfRatings(idCar);

    res.status(200);
    res.send({ rating, numValoraciones });

    // SEGUNDA - Con Js
    // const reviews = await carsRepository.findReviewsByCarId(id);
    // const sumasReviews = reviews.reduce((acc, review) => {
    //   return acc + review.rating;
    // }, 0);
    // const ratingJs = sumasReviews / reviews.length;
    // console.log('Rating JS', ratingJs);
    // res.status(200);
    // res.send({
    //   rating: ratingJs,
    //   numValoraciones: reviews.length
    // });

  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = { getCarAverageRatingById };

'use strict';

const Joi = require('joi');
const { findAllReviews } = require('../../repositories/reviews-repository');
const createJsonError = require('../../errors/create-json-error');

async function getReviews(req, res) {
  try {
    if (req.auth.rol !== 'admin') {
      const error = new Error('No tienes permisos para realizar esta acción');
      error.status = 403;
      throw error;
    }
    const reviews = await findAllReviews();

    res.send(reviews);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = getReviews;

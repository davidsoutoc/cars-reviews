'use strict';

const Joi = require('joi');
const reviewsRepository = require('../../repositories/reviews-repository');
const createJsonError = require('../../errors/create-json-error');

const schema = Joi.number().positive().required();

async function deleteReview(req, res) {
  try {
    const { id } = req.params;
    await schema.validateAsync(id);

    const review = await reviewsRepository.findReviewById(id);
    if (!review) {
      const error = new Error('Review no existe');
      error.status = 400;
      throw error;
    }

    if (!req.auth.rol === 'admin' || !review.user_id === id) {
      const error = new Error('No tienes permisos para realizar esta acción');
      error.status = 403;
      throw error;
    }

    await reviewsRepository.deleteReviewById(id);

    res.status(200);
    res.send({ message: `Review id:${id} borrada` });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = deleteReview;

'use strict';

const Joi = require('joi');
const { findCarById } = require('../../repositories/cars-repository');
const { addReview } = require('../../repositories/reviews-repository');
const createJsonError = require('../../errors/create-json-error');
const throwJsonError = require('../../errors/throw-json-error');

const schema = Joi.object().keys({
  carId: Joi.number().positive().required(),
  comment: Joi.string().min(5).max(255),
  rating: Joi.number().min(0).max(10).required(),
});

async function createReview(req, res) {
  try {
    const { id } = req.auth;

    await schema.validateAsync(req.body);
    const { carId, comment, rating } = req.body;

    const car = await findCarById(carId);
    if (!car) {
      throwJsonError('Coche no existe', 400);
    }

    const idReview = await addReview(id, carId, comment, rating);

    res.status(200);
    res.send({ idReview, carId, comment, rating });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = createReview;

'use strict';

const express = require('express');
const router = express.Router();
const { getCars } = require('../controllers/cars/get-cars');
const { getCarById } = require('../controllers/cars/get-car-by-id');
const { createCar } = require('../controllers/cars/create-car');
const { deleteCarById } = require('../controllers/cars/delete-car-by-id');
const { updateCarById } = require('../controllers/cars/update-car-by-id');
const { patchCarById } = require('../controllers/cars/patch-car-by-id');
const { getCarReviewsById} = require('../controllers/cars/get-reviews-by-car-id');
const { getCarAverageRatingById } = require('../controllers/cars/get-car-average-rating-by-id');
const validateAuth = require('../middlewares/validate-auth');

// Endpoint Públicos
router.route('/').get(getCars);
router.route('/:idCar').get(getCarById);
router.route('/:idCar/reviews').get(getCarReviewsById);
router.route('/:idCar/rating').get(getCarAverageRatingById);

// Endpoint Privados
router.route('/')
  .all(validateAuth)
  .post(createCar);

router.route('/:idCar')
  .all(validateAuth)
  .put(updateCarById)
  .patch(patchCarById)
  .delete(deleteCarById);


module.exports = router;
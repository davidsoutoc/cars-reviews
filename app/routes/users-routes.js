'use strict';

const express = require('express');
const router = express.Router();
const { activateUser } = require('../controllers/users/activation-account');
const { getUsers } = require('../controllers/users/get-users');
const { loginUser } = require('../controllers/users/login-user');
const { registerUser } = require('../controllers/users/register-user');
const { updateUser } = require('../controllers/users/update-user');
const { uploadImageProfile } = require('../controllers/users/upload-image-profile');
const { getUserProfile } = require('../controllers/users/get-user-profile');
const { deleteUserById } = require('../controllers/users/delete-user-by-id');
const { getUserReviewsById } = require('../controllers/users/get-users-reviews-by-id');
const { randomUser } = require('../controllers/users/random-user');
const validateAuth = require('../middlewares/validate-auth');

//Publicas
///api/v1/users
router.route('/register').post(registerUser);
router.route('/login').post(loginUser);
router.route('/activation').get(activateUser);
router.route('/random').get(randomUser);
//Privadas
router
  .route('/')
  .all(validateAuth)
  .get(getUsers)
  .put(updateUser);
router
  .route('/:id')
  .all(validateAuth)
  .delete(deleteUserById);
router
  .route('/:id/reviews')
  .all(validateAuth)
  .get(getUserReviewsById);
router
  .route('/profile')
  .all(validateAuth)
  .get(getUserProfile);
router
  .route('/upload')
  .all(validateAuth)
  .post(uploadImageProfile);

module.exports = router;

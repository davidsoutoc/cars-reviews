'use strict';

const express = require("express");
const router = express.Router();
const axios = require("axios");
const cheerio = require("cheerio");

const { getTitularesElPais } = require('../controllers/titulares/get-titulares-elpais');
const { getTitularesMarca } = require('../controllers/titulares/get-titulares-marca');

router.route('/elpais').get(getTitularesElPais);
router.route('/marca').get(getTitularesMarca);

module.exports = router;
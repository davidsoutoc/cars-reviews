'use strict';

const express = require('express');

const validateAuth = require('../middlewares/validate-auth');
const createReview = require('../controllers/reviews/create-review');
const deleteReview = require('../controllers/reviews/delete-review');
const getReviews = require('../controllers/reviews/get-reviews');

const router = express.Router();

//Privadas
router
  .route('/')
  .all(validateAuth)
  .get(getReviews)
  .post(createReview);
router
  .route('/:id')
  .all(validateAuth)
  .delete(deleteReview);

module.exports = router;

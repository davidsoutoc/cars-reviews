'use strict';

const database = require('../infrastructure/database');

async function addReview(idUser, idCar, comment, rating) {
  const pool = await database.getPool();
  const now = new Date();
  const insertQuery = `INSERT
    INTO reviews (idUser, idCar, comment, rating, createdAt)
    VALUES (?, ?, ?, ?, ?)`;
  const [created] = await pool.query(
    insertQuery, [idUser, idCar, comment, rating, now]
  );

  return created.insertId;
}

async function deleteReviewById(id) {
  const pool = await database.getPool();
  const query = 'DELETE FROM reviews WHERE id = ?';
  const [reviews] = await pool.query(query, id);

  return reviews;
}

async function findReviewById(id) {
  const pool = await database.getPool();
  const query = 'SELECT * FROM reviews WHERE id = ?';
  const [reviews] = await pool.query(query, id);

  return reviews;
}

async function findReviewsByCarId(idCar) {
  const pool = await database.getPool();
  const query = 'SELECT * FROM reviews WHERE idCar = ?';
  const [reviews] = await pool.query(query, idCar);

  return reviews;
}

async function findReviewsByUserId(idUser) {
  const pool = await database.getPool();
  const query = `SELECT reviews.*, cars.marca, cars.modelo, cars.anho
    FROM reviews
    LEFT JOIN cars ON cars.id = reviews.idCar
    WHERE idUser = ?`;
  const [reviews] = await pool.query(query, idUser);

  return reviews;
}

async function findAllReviews() {
  const pool = await database.getPool();
  //const query = 'SELECT * FROM reviews';
  const query = `SELECT reviews.*, users.nombre, cars.marca, cars.modelo, cars.anho
    FROM reviews
    INNER JOIN users ON users.id = reviews.idUser
    INNER JOIN cars ON cars.id = idCar`;
  const [reviews] = await pool.query(query);

  return reviews;
}

async function getAverageRating(idCar) {
  const pool = await database.getPool();
  const query = 'SELECT AVG(rating) as valoracionMedia FROM reviews WHERE idCar = ?';
  const [valoracion] = await pool.query(query, idCar);

  return valoracion[0].valoracionMedia;
}

async function getNumberOfRatings(idCar) {
  const pool = await database.getPool();
  const query = 'SELECT COUNT(rating) as numValoraciones FROM reviews WHERE idCar = ?';
  const [valoracion] = await pool.query(query, idCar);

  return valoracion[0].numValoraciones;
}

module.exports = {
  addReview,
  deleteReviewById,
  findAllReviews,
  findReviewById,
  findReviewsByCarId,
  findReviewsByUserId,
  getAverageRating,
  getNumberOfRatings,
};

'use strict';

const database = require('../infrastructure/database');

async function findAllCars() {
  const pool = await database.getPool();
  const consulta = 'SELECT * FROM cars WHERE deletedAt IS NULL';
  const [cars] = await pool.query(consulta);

  return cars;
}

async function findCarById(idCar) {
  const pool = await database.getPool();
  const consulta = `SELECT * FROM cars WHERE id = ? AND deletedAt IS NULL`;
  const [car] = await pool.query(consulta, idCar);
  console.log(car);

  return car[0];
}
async function addCar(car) {
  const pool = await database.getPool();
  const now = new Date();
  const consulta = `INSERT INTO cars(
    marca,
    modelo,
    anho,
    motor,
    cv,
    createdAt
    ) VALUES (?, ?, ?, ?, ?, ?)`;

  const [created] = await pool.query(consulta, [
    ...Object.values(car),
    now
  ]);
  // const { marca, modelo, anho, motor, cv } = car;
  // const [created] = await pool.query(consulta, [
  //   marca,
  //   modelo,
  //   anho,
  //   motor,
  //   cv,
  //   now
  // ]);

  //return created.insertId;
  return true;
}
async function logicRemoveCarById(idCar) {
  const pool = await database.getPool();
  const consulta = 'UPDATE cars SET deletedAt = ? WHERE id = ?';
  const now = new Date();
  await pool.query(consulta, [now, idCar]);

  return true;
}

async function removeCarById(idCar) {
  const pool = await database.getPool();
  const consulta = 'DELETE FROM cars WHERE id = ?';
  await pool.query(consulta, idCar);

  return true;
}

async function findCarByBrandAndModel(marca, modelo) {
  const pool = await database.getPool();
  const query = 'SELECT * FROM cars WHERE marca = ? AND modelo = ?';
  const [cars] = await pool.query(query, [marca, modelo]);

  return cars[0];
}

async function updateCar(id, car) {
  const { marca, modelo, anho, motor, cv } = car;
  const now = new Date();
  const pool = await database.getPool();
  const updateQuery = `
    UPDATE cars
    SET marca = ?, modelo = ?, anho = ?, motor = ?, cv = ?, updatedAt = ?
    WHERE id = ?`;
  await pool.query(updateQuery, [marca, modelo, anho, motor, cv, now, id]);

  return true;
}

async function findReviewsByCarId(carId) {
  const pool = await database.getPool();
  const query = `
    SELECT cars.marca, cars.modelo, reviews.comment,
    reviews.rating, reviews.createdAt, users.nombre
    FROM cars
    LEFT JOIN reviews ON reviews.idCar = cars.id
    INNER JOIN users ON reviews.idUser = users.id
    WHERE cars.id = ?`;
  const [reviews] = await pool.query(query, carId);

  return reviews;
}

module.exports = {
  addCar,
  findAllCars,
  findCarByBrandAndModel,
  findCarById,
  findReviewsByCarId,
  logicRemoveCarById,
  removeCarById,
  updateCar,
};

'use strict';

require('dotenv').config();
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');
const fileUpload = require('express-fileupload');
const app = express();

// Para subir ficheros
app.use(fileUpload());
// Para mostrar ficheros, imagenes, css... en carpeta public
app.use(express.static('public'));
// Recibir datos como json en el body
app.use(express.json());
// CORS - dar permisos de acceso a otras URLs
app.use(cors());

const carsRouter = require('./app/routes/cars-routes');
const usersRouter = require('./app/routes/users-routes');
const reviewsRouter = require('./app/routes/reviews-routes');
const titularesRouter = require('./app/routes/titulares-routes');

const port = process.env.SERVER_PORT || 3000;

// LOG con Morgan
const accessLogStream = fs.createWriteStream(path.join(__dirname, './access.log'), { flags: 'a' });
app.use(morgan('combined', { stream: accessLogStream }));

app.use('/api/v1/cars/', carsRouter);
app.use('/api/v1/users/', usersRouter);
app.use('/api/v1/reviews/', reviewsRouter);
app.use('/api/v1/titulares/', titularesRouter);

app.listen(port, () => console.log(`Escuchando puerto ${port}`));

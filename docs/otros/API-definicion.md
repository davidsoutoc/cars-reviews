# CARS-ROUTES						# CARS-CONTROLLERS
GET 	/api/v1/cars 				-> getCars				-> Publica 	-> Listado de los coches
GET 	/api/v1/cars/:id			-> getCarById			-> Publica	-> Información de un coche
GET 	/api/v1/cars/:id/reviews	-> getCarReviewsById	-> Publica	-> Reviews de un coche por ID
GET 	/api/v1/cars/:id/rating		-> getCarRatingById		-> Publica	-> Valoración media de los usuarios y números de valoraciones

POST	/api/v1/cars 				-> createCar			-> Admin	-> Crear un coche
PUT		/api/v1/cars/:id 			-> updateCarById		-> Admin	-> Actualizar un coche por ID
PATCH	/api/v1/cars/:id 			-> patchCarById			-> Admin	-> Actualizar un campo de un coche por ID
DELETE	/api/v1/cars/:id 			-> deleteCarById		-> Admin	-> Eliminar un coche por ID

# REVIEWS-ROUTES					# REVIEWS-CONTROLLERS
POST	/api/v1/reviews 			-> createReview			-> Privada	-> Crear una review de un coche. OJO el idCar va en el body
GET 	/api/v1/reviews 			-> getReviews			-> Admin 	-> Listado de las reviews, solo disponible para admin
DELETE  /api/v1/reviews/:id 		-> deleteReview			-> Admin 	-> Solo el admin puede eliminar reviews

# USERS-ROUTES						# USERS-CONTROLLERS
POST	/api/v1/users/register		-> registerUser			-> Publica	-> Registrar un usuario nuevo
POST	/api/v1/users/login			-> loginUser			-> Publica	-> Loguearse en la aplicacion
POST	/api/v1/users/activation	-> activateUser			-> Publica	-> Link para activar las cuentas
PUT		/api/v1/users 				-> updateUser			-> Privada	-> La ejecuta el propio usuario (obtiene el ID del accessToken)
POST	/api/v1/user/upload			-> uploadImageProfile 	-> Privada	-> El propio usuario puede cambiar el avatar


GET 	/api/v1/users/profile		-> getUserProfile		-> Privada	-> Perfil del usuario. CADA USUARIO VE EL SUYO
GET		/api/v1/users 				-> getUsers				-> Admin 	-> Listado usuarios
GET 	/api/v1/user/:id 			-> getUserReviewsById	-> Admin 	-> Ver reviews de un usuario
DELETE	/api/v1/users/:id 			-> deleteUserById		-> Admin 	-> Eliminar un usuario. SOLO el ADMIN puede


---> Ruta pública de entrega de imagenes de perfiles
http://localhost:3000/images/profiles/14.jpeg
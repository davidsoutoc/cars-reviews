CREATE DATABASE IF NOT EXISTS cars_reviews;
USE cars_reviews;

CREATE TABLE IF NOT EXISTS cars (
	id INT NOT NULL AUTO_INCREMENT,
    marca VARCHAR(100) NOT NULL,
    modelo VARCHAR(255) NOT NULL,
    anho INT NOT NULL,
    motor ENUM('Diésel', 'Gasolina', 'Híbrido', 'Eléctrico') DEFAULT 'Gasolina',
    cv INT NULL,
    createdAt DATETIME NOT NULL,
    updatedAt DATETIME NULL,
    deletedAt DATETIME NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS users(
	id INT NOT NULL AUTO_INCREMENT,
    nombre varchar(255) NOT NULL,
    email varchar(250) NOT NULL,
    password VARCHAR(128) NOT NULL,
    rol ENUM('admin', 'reader') DEFAULT 'reader',
    image VARCHAR(255) NULL,
    createdAt DATETIME NOT NULL,
    updatedAt DATETIME NULL,
	verificationCode VARCHAR(64) NULL,
	verifiedAt DATETIME NULL,
	PRIMARY KEY (id)
);

/*
ALTER TABLE users ADD verificationCode VARCHAR(64) AFTER updatedAt;
ALTER TABLE users ADD verifiedAt DATETIME AFTER verificationCode;
*/
CREATE TABLE IF NOT EXISTS reviews (
  id int NOT NULL AUTO_INCREMENT,
  idUser int NOT NULL,
  idCar int NOT NULL,
  comment varchar(255) NOT NULL,
  rating int NOT NULL,
  createdAt DATETIME NOT NULL,
  updatedAt DATETIME NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`idUser`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`idCar`) REFERENCES `cars` (`id`) ON DELETE CASCADE
)